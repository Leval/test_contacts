virtualenv virtualenv/test_contacts

pip install -r requirements.txt

source virtualenv/test_contacts/bin/activate

python manage.py syncdb
python manage.py migrate contacts