# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from web.contacts_web.views.ContactDetailsView import ContactDetailsView
from web.contacts_web.views.IndexView import IndexView


index_view = IndexView.as_view()
contact_details = ContactDetailsView.as_view()