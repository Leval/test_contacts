# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from web.contacts_web.facades.ContactsFacade import ContactsFacade


class IndexView(TemplateView):
    template_name = 'test_contacts/index_page.html'
    contacts_facade = ContactsFacade()

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data()
        context['users'] = self.contacts_facade.user_service.get_all()
        return context