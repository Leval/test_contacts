# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from web.contacts_web.facades.ContactsFacade import ContactsFacade


class ContactDetailsView(TemplateView):
    template_name = 'test_contacts/contact_details.html'
    contacts_facade = ContactsFacade()

    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        context['contact'] = self.contacts_facade.get_user_contact(kwargs['pk'])
        return context