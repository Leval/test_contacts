# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.utils.translation import ugettext_lazy as _
from web.contacts_web.views import contact_details

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'test_contacts.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'(?P<pk>\d+)/$', contact_details, name='contact_page'),
)