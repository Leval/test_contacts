# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from core.contacts.services.ContactService import ContactService
from core.main.services.UserService import UserService


class ContactsFacade(object):
    base_service = ContactService()
    user_service = UserService()

    def get_users_with_contact(self):
        users = self.user_service.get_all()
        return users

    def get_user_contact(self, user_id):
        user = self.user_service.get_by_id(user_id)
        try:
            return self.base_service.get_user_contact(user)[0]
        except IndexError:
            return None