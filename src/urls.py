from django.conf.urls import patterns, include, url

from django.contrib import admin
from web.contacts_web.views import index_view

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_contacts.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^contact/', include('web.contacts_web.urls', namespace='contacts')),
    url(r'^$', index_view, name='index_page'),

)
