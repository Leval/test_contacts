# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class UserService(object):
    model_instance = User

    def get_by_id(self, id):
        return self.model_instance.objects.get(id=id)

    def get_all(self):
        return self.model_instance.objects.all().order_by()