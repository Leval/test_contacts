# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from core.contacts.models import Contact


admin.site.register(Contact)