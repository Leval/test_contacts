# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from core.contacts.models import Contact


class ContactService(object):
    model_instance = Contact

    def get_by_id(self, id):
        return self.model_instance.objects.get(id=id)

    def get_all(self):
        return self.model_instance.objects.all().order_by()

    def get_user_contact(self, user):
        return self.model_instance.objects.filter(owner=user).order_by('-id')