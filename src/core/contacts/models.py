# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Contact(models.Model):
    owner = models.ForeignKey(User)
    phone = models.CharField(max_length=12, blank=True, null=True)
    home_phone = models.CharField(max_length=12, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return "ID: %s - Username: %s" % (self.pk, self.owner.username)
